# Lab1

## Part 1

### 1.1
Access the `time_in_usec` inside the struct `CnetNodeInfo`.

CnetTime is an alias of `int64_t`, therefore,

``` C
printf("time = %lld\n", nodeinfo.time_in_usec);
```

### 1.2

Code from `ping-W17.c`
```C
delta = nodeinfo.time_in_usec - f.time_send;
```
Given to the output, delta = 211428 usec. The delta is the delay for the node to send out a `HELLO` and receive a `HELLO_ACK` confirmation. It may affects by the propagation delay, bandwith, etc.

In order to make delta approximately equal to 1 second, I changes the `wan-propagationdelay` to 500ms.

### 1.3
```C
typedef struct {
    CnetTimerID lasttimer;
    int src, dest;
    CnetPosition src_pos, dest_pos;
    uint16_t checksum;
} PACKET;
```

### 1.4
It is possible, since `FRAME_SIZE(f)` is the size of the frame header plus the message length. On the other hand, `sizeof(f)` provides the size of the frame header plus the maximum message size. In other words, `FRAME_SIZE(f)` gives the actual size of the frame is used, and `sizeof(f)` gives the maxmimum size of the frame.

### 1.5
msg1 checksum: 65514
msg2 checksum: 65514

### 1.6
msg1: 50874
msg2: 56590

### 1.7
It will call CNET_exit for `__FILE__`, `__func__`, and `__LINE__` in order to know which file, which method causes an error. Those information will be showed on a new pop-op window.

## Part 2

### Design Overview
To implement a Neighbours Discovery Protocol, since cnet does not comes with built-in way to get information of linked noded, necessary information has to be sent with and exchange between everty linked nodes. My own implementation is, host will keep sending a `HELLO` message to the linked node along with the its hostname and address. In return, the host that receives the `HELLO` message will send a `HELLO_ACK` message along with its hostname and address back to the host which sends out the `HELLO` message. By keep those information in the memory, the host will be able to retrive information of every linked nodes. Meanwhile, the host will count the number of times receive those messages.

### Program Status
The program works perfectly fine and meet the requirement. It takes some time to understand how to programming with CNET API.

### Acknowledgments
* [CNET Documentation](www.csse.uwa.edu.au/cnet/api.html)
