#include <cnet.h>
#include <assert.h>
#include <string.h>

// This file is based on "protocol.c"
// ------------------------------

typedef enum { DL_HELLO, DL_HELLO_ACK } FRAMEKIND;
typedef struct {
    char         data[20];
} MSG;

typedef struct {
    FRAMEKIND    kind;
    int          srcNodeNumber;
    CnetAddr     srcAddr;
    char         srcNodeName[MAX_NODENAME_LEN];
    CnetAddr     destAddr;
    char         destNodeName[MAX_NODENAME_LEN];
    CnetTime     time_send;
    MSG          msg;
} FRAME;

// store linked node info
char linkedNodeName[32][MAX_NODENAME_LEN];
CnetAddr linkedNodeAddr[32];

// Counter
int counter = 0;
int num_hello = 0;
int num_hello_ack = 0;

// --------------------
static EVENT_HANDLER(application_ready)
{
    CnetAddr	destaddr;
    char	buffer[MAX_MESSAGE_SIZE];
    size_t	length;

    length = sizeof(buffer);
    CNET_read_application(&destaddr, buffer, &length);
    printf("\tI have a message of %4d bytes for address %d\n",
			    length, (int)destaddr);
}
// ------------------------------

static EVENT_HANDLER(button_pressed)
{
    printf("\n Number of received HELLO msg: %d\n", num_hello);
    printf("Number of HELLO_ACK sent: %d\n", num_hello_ack);
    //printf("numer of links: %d\n", nodeinfo.nlinks);

    int i = 0;
    for (i = 0; i < counter; ++i) {
        printf("link %d -> %s, %d\n", i+1, linkedNodeName[i], linkedNodeAddr[i]);
    }
}

bool contains(CnetAddr addr) {
    int i = 0;
    for (i = 0; i < nodeinfo.nlinks; ++i) {
        if (linkedNodeAddr[i] == addr) return true;
    }
    return false;
}

// ------------------------------
static void physical_ready(CnetEvent ev, CnetTimerID timer, CnetData data)
{
    int    link;
    size_t len;
    FRAME  f;

    len = sizeof(FRAME);
    CHECK ( CNET_read_physical (&link, (char *) &f, &len) );

    switch (f.kind) {
        case DL_HELLO:
            //assert ( link == 1 );
            f.kind = DL_HELLO_ACK;
            num_hello += 1; // increase counter
            // echo on the same link
            len = sizeof(f);

            // add cureent node info to the fram
            f.destAddr = nodeinfo.address;
            memcpy(f.destNodeName, nodeinfo.nodename, sizeof(nodeinfo.nodename));
            // printf("Received HELLO from: %d %s\n", f.srcAddr, f.srcNodeName);

            // store the info of neighbour node
            if (contains(f.srcAddr) == false) {
                linkedNodeAddr[counter] = f.srcAddr;
                memcpy(linkedNodeName[counter], f.srcNodeName, sizeof(f.srcNodeName));
                // printf("Connected! %s %d %d\n", f.srcNodeName, f.srcAddr, counter);
                ++counter;
            }

            CHECK( CNET_write_physical(link, (char *) &f, &len) );
            break;

        case DL_HELLO_ACK:
            assert ( f.srcAddr == nodeinfo.address );
            // printf("Receive HELLO_ACK from: %d %s\n", f.destAddr, f.destNodeName);
            num_hello_ack += 1; // increase counter
            // store info
            if (contains(f.destAddr) == false) {
                linkedNodeAddr[counter] = f.destAddr;
                memcpy(linkedNodeName[counter], f.destNodeName, sizeof(f.destNodeName));
                //printf("Connected! %s %d %d\n", f.destNodeName, f.destAddr, counter);
                ++counter;
            }

            break;
    }
}

// Recall: the macro expands to the following function declaration:
// static void timer1_send_hello (CnetEvent ev, CnetTimerID timer, CnetData data)
EVENT_HANDLER(timer1_send_hello)
{
    int   link=1;
    size_t  len;
    FRAME f;

    f.kind = DL_HELLO;
    f.srcAddr = nodeinfo.address;

    // copy nodenmae into the frame
    memcpy(f.srcNodeName, nodeinfo.nodename, sizeof(nodeinfo.nodename));
    f.time_send =  nodeinfo.time_in_usec;    // get current time

    len = sizeof(f);

    // send HELLO to every neighbour
    for (link = 1; link < nodeinfo.nlinks ; ++link) {
        CHECK( CNET_write_physical(link, (char *) &f, &len) );
    }

    CNET_start_timer (EV_TIMER1, 1000000, 0);   // send continuous hello
}

// ------------------------------
EVENT_HANDLER(reboot_node)
{
//  Interested in hearing about:
//    - the Application Layer having messages for delivery
//    - timer 1
//

    CNET_set_handler(EV_APPLICATIONREADY, application_ready, 0);

    CNET_set_handler(EV_DEBUG0, button_pressed, 0);
    CNET_set_debug_string(EV_DEBUG0, "Node Info");

    CNET_set_handler(EV_PHYSICALREADY, physical_ready, 0);
    CNET_set_handler(EV_TIMER1, timer1_send_hello, 0);

    // Request EV_TIMER1 in 1 sec, ignore return value
    CNET_start_timer (EV_TIMER1, 1000000, 0);

    //  CNET_enable_application(ALLNODES);
}
